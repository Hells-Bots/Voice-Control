from discord import Member, Channel, PermissionOverwrite, NotFound

from bot import VoiceControlBot


async def give_member_overwrites(bot: VoiceControlBot, member: Member, vc: Channel):
    tc = bot.get_channel(bot.settings.get_tc_for_vc(vc))
    if tc is None:
        return

    overwrites_list = bot.settings.get_overwrites()
    overwrites_dict = {}
    for item in overwrites_list:
        overwrites_dict.update(item)

    new_overwrite = PermissionOverwrite()
    new_overwrite.update(**overwrites_dict)

    try:
        await bot.edit_channel_permissions(tc, member, new_overwrite)
    except NotFound:
        pass  # The member probably got banned, so we don't care

async def remove_member_overwrites(bot: VoiceControlBot, member: Member, vc: Channel):
    tc = bot.get_channel(bot.settings.get_tc_for_vc(vc))
    if tc is None:
        return

    try:
        await bot.delete_channel_permissions(tc, member)
    except NotFound:
        pass

async def update_all_member_overwrites(bot: VoiceControlBot, vc: Channel):
    vc_member_ids = [m.id for m in vc.voice_members]

    tc = bot.get_channel(bot.settings.get_tc_for_vc(vc))
    if tc is not None:
        for target, overwrite in tc.overwrites:
            if type(target) == Member:
                if target.id not in vc_member_ids:
                    await remove_member_overwrites(bot, target, vc)

    for member in vc.voice_members:
        await give_member_overwrites(bot, member, vc)
