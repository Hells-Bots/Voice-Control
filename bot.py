import logging
import traceback
import os

from raven import fetch_git_sha
from raven.handlers.logging import Client as RavenClient

from discord import Client as DiscordClient, errors, Message, Member

from custom_exceptions import StyledUserNotification
from settings_service import Settings


class VoiceControlBot(DiscordClient):
    def __init__(self, **options):
        super().__init__(**options)

        self.settings = Settings()

        root_logger = logging.getLogger()
        root_logger.setLevel(logging.INFO)

        f_handler = logging.FileHandler('run.log', 'w', encoding='UTF-8')
        formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(name)s | %(message)s')
        f_handler.setFormatter(formatter)
        root_logger.addHandler(f_handler)

        if self.settings.get_sentry_dsn() is not None:
            self.r_client = RavenClient(
                self.settings.get_sentry_dsn(),
                auto_log_stacks=True,
                release=fetch_git_sha(os.path.dirname(__file__))
            )
        else:
            self.r_client = None

    def startup(self):
        token = self.settings.get_token()

        try:
            self.loop.run_until_complete(self.start(token))
        except errors.LoginFailure:
            raise RuntimeError('Please fix the given Token-Value inside settings.yaml!')

    async def on_ready(self):
        await self.change_presence(
            status=None  # = online
        )


    async def on_message(self, message: Message):
        await self.wait_until_ready()

        # Preflight - Checks:
        if message.author == self.user:
            return

        if message.server is None:
            return

        content = message.content.strip()
        if not content.startswith(self.settings.get_prefix()):
            return

        # Unpacking:
        command, *args = content.split(self.settings.get_prefix(), 1)[-1].split(' ')

        if len(command) == 0:
            return

        # Get Handler:
        try:
            import commands
            handler = getattr(commands, 'cmd_' + command, None)
        except BaseException as ex:
            logging.error('Error while trying to load the command handlers: ' + str(ex))
            return

        if handler is None:
            return

        try:
            # Okay, let's do this!
            logging.info('got cmd: s: "%s" - c: "%s" - u: "%s %s" - m: "%s"'%(
                message.server.name, message.channel.name, message.author.display_name, message.author.mention, content
            ))
            await self.send_typing(message.channel)
            await handler(self, message, command, args)
            logging.info('cmd done')

        except StyledUserNotification as sun:
            logging.info('cmd done, with User Notification "' + str(sun) + '"')

            await self.send_message(
                message.channel, str(sun)
            )

        except BaseException as ex:
            if self.r_client:
                self.r_client.captureException(exec_info=True)
            logging.error(str(ex))
            logging.error(traceback.format_exc())

            await self.send_message(
                message.channel, '¯\\_(ツ)_/¯  ' + str(ex)
            )

    async def on_voice_state_update(self, before: Member, after: Member):
        if before.voice.voice_channel == after.voice.voice_channel:
            return

        from handlers import handle_vc_update
        await handle_vc_update(self, before, after)
