import asyncio
import logging
import traceback

from discord import Member, Channel, Message, NotFound

from bot import VoiceControlBot
from utils import remove_member_overwrites, give_member_overwrites


async def handle_vc_update(bot: VoiceControlBot, before: Member, after: Member):
    await _handle_wrapped(_update_roles, bot, before, after)
    await _handle_wrapped(_send_update_msg, bot, before, after)

async def _handle_wrapped(method, bot: VoiceControlBot, before: Member, after: Member):
    try:
        await method(bot, before, after)
    except BaseException as ex:
        if bot.r_client:
            bot.r_client.captureException(exec_info=True)
        logging.error(str(ex) + ', while executing ' + method.__name__)
        logging.error(traceback.format_exc())


async def _update_roles(bot: VoiceControlBot, before: Member, after: Member):
    before_vc = before.voice.voice_channel
    after_vc = after.voice.voice_channel

    if before_vc != after_vc:
        if before_vc:
            await remove_member_overwrites(bot, before, before_vc)
        if after_vc:
            await give_member_overwrites(bot, after, after_vc)

async def _send_update_msg(bot: VoiceControlBot, before: Member, after: Member):
    before_vc = before.voice.voice_channel
    after_vc = after.voice.voice_channel

    name = before.display_name

    if before_vc and after_vc:
        message1 = await __send_msg_to_tc(
            bot, before_vc, ':arrow_left:  %s, zu %s' % (
                name, after_vc.name
            )
        )
        message2 = await __send_msg_to_tc(
            bot, after_vc, ':arrow_right:  %s, von %s' % (
                name, before_vc.name
            )
        )

        await asyncio.sleep(60)
        await __delete(bot, message1)
        await __delete(bot, message2)

    elif before_vc:
        message = await __send_msg_to_tc(
            bot, before_vc, ':arrow_left:  %s' % name
        )

        await asyncio.sleep(60)
        await __delete(bot, message)

    elif after_vc:
        message = await __send_msg_to_tc(
            bot, after_vc, ':arrow_right:  %s' % name
        )

        await asyncio.sleep(60)
        await __delete(bot, message)


async def __send_msg_to_tc(bot: VoiceControlBot, vc: Channel, text: str):
    if vc is None:
        return None

    tc_id = bot.settings.get_tc_for_vc(vc)
    if tc_id is None:
        return None

    return await bot.send_message(bot.get_channel(tc_id), text)

async def __delete(bot: VoiceControlBot, message: Message):
    if message is None:
        return

    try:
        await bot.delete_message(message)
    except NotFound:
        pass
