import logging

from bot import VoiceControlBot


bot = VoiceControlBot()
try:
    bot.startup()
except KeyboardInterrupt:
    try:
        logging.info('Got KeyboardInterrupt, kthxbai')
    except:
        pass
