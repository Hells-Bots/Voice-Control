class StyledUserNotification(BaseException):
    """
    Notifies the user about something they did wrong.
    Not logged and not handled as an error.
    """
    pass

class NothingFoundError(BaseException):
    pass
