from discord import Message, ChannelType

from bot import VoiceControlBot
from utils import update_all_member_overwrites


async def cmd_refresh(bot: VoiceControlBot, message: Message, command: str, args: [str]):
    for channel in message.server.channels:
        if channel.type == ChannelType.voice:
            await update_all_member_overwrites(bot, channel)

    await bot.send_message(message.channel, ':ok_hand:')
