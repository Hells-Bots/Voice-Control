from typing import Union

import yaml
from discord import Channel


class ConfigError(ValueError):
    pass


class Settings:
    def __init__(self):
        with open('settings.yaml') as f:
            self.data = yaml.safe_load(f)

    def get_sentry_dsn(self) -> Union[None, str]:
        if 'sentry_dsn' not in self.data:
            return None

        return self.data['sentry_dsn']

    def get_token(self) -> str:
        if 'token' not in self.data:
            raise ConfigError('A `token` needs to be defined in settings.yaml!')

        return self.data['token']

    def get_prefix(self) -> str:
        if 'prefix' not in self.data:
            return '-'

        return self.data['prefix']

    def get_mapping(self) -> [{}]:
        if 'mapping' not in self.data:
            raise ConfigError('The desired `mapping` must be specified in settings.ymal!')

        return self.data['mapping']

    def get_tc_for_vc(self, vc: Channel):
        for map in self.get_mapping():
            if map['voice'] == vc.id:
                return map['text']

        return None

    def get_vc_for_tc(self, tc: Channel):
        for map in self.get_mapping():
            if map['text'] == tc.id:
                return map['voice']

        return None

    def get_overwrites(self) -> [{}]:
        if 'overwrites' not in self.data:
            raise ConfigError('The desired `overwrites` must be specified in settings.ymal!')

        return self.data['overwrites']
